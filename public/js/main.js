let socket=io("wss://lcoapi-forum-miceve.cloud.okteto.net/api");
let recaptchakey="6LdO19YUAAAAAP3n8__ewdXh_X2Z1XnxfmwmW1GL";
if(window.location.hash.length===0){
    loadMain()

}else{
    let hash=window.location.hash;
    hash=hash.split("#");
    hash.splice(0,1);
    if(hash[0]===""){
        loadMain()
    }else if(hash[0]==="show"){
        document.getElementById(hash[1]).style.visibility="visible"
    }else if(hash[0]==="close"){
        document.getElementById(hash[1]).style.visibility="hidden"
    } else if(hash[0]==="main"){
        loadMain()
    } else if(hash[0]==="logininit"){
        let email=document.getElementById("login-email").value
        let password=document.getElementById("login-password").value
        socket.emit("getkey",email,password)
        socket.on("apikey",(failed,key)=>{
            if(!failed){
                window.localStorage.setItem("apikey",key)
                socket.emit("auth",key)
            }else{
                document.getElementById("login").style.backgroundColor="red"
                setTimeout(()=>{
                    document.getElementById("login").style.backgroundColor="#323a45"
                },2500)
            }
        })
    }
}
window.onhashchange=(uri=>{
let hash=window.location.hash;
    hash=hash.split("#");
    hash.splice(0,1);

    if(hash[0]===("post")){
        let id=hash.split("#")[1];
        socket.emit("get-post",id);
        socket.once("post"+id,(content)=>{

        })
    }else if(hash[0]==="show"){
    document.getElementById(hash[1]).style.visibility="visible"
    }else if(hash[0]==="close"){
        document.getElementById(hash[1]).style.visibility="hidden"
    }else if(hash[0]===""){
        loadMain()
    } else if(hash[0]==="main"){
        loadMain()
    }
});
function loadMain() {

    socket.emit("get-posts");
    socket.on("posts",posts=>{
        document.querySelector("#landpage").innerHTML="";
        console.log(posts);
        posts.forEach(post=>{
            let postEl=document.createElement("div");
            postEl.className="card";
            postEl.style.background=`url("${post.img}")`;

            let postPreview=document.createElement("div");
            postPreview.className="card-preview";
            postPreview.innerHTML=post.raw
                .split("")
                .slice(0,30).filter(l=>{return !["*","#","`","~","_","-","^","="].includes(l)})
                .join("")+"...";
            let postTitle=document.createElement("a");
            postTitle.className="card-title";
            postTitle.innerHTML=post.title;
            let postGo=document.createElement("a");
            postGo.className="card-go";
            postGo.innerHTML="Читать";
            postGo.href="#post#"+post._id;
            document.querySelector("#landpage").appendChild(postEl);
            postEl.appendChild(postTitle);
            postEl.appendChild(postPreview);
            postEl.appendChild(postGo)

            //     <div class="card">
            //     <a class="card-title">Lorem ipsum</a>
            // <div class="card-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
            // <div class="card-go">Читать</div>
            //     </div>
        })
    })
}