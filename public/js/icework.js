let IW={}
IW.Ticker=[]
IW.Actions={}
IW.Init=function IWInit(rootElem) {
  for (let i = 0; i < rootElem.children.length; i++) {
  let  el=rootElem.children[i]
  IW.Init(el)
  if(el.getAttribute("iw-link")!=null){
    el.style.cursor="pointer"
    el.addEventListener("click",()=>{
      window.open(el.getAttribute("iw-link"))
    })
  }
  if(el.getAttribute("iw-hidden")!=null){
    if(el.getAttribute("iw-hidden")=="true"){
    el.style.visibility="hidden";
  }else if(el.getAttribute("iw-hidden")=="false"){
      el.style.visibility="visible";
  }else{
      el.style.visibility="hidden";
  }
  }

  if(el.getAttribute("iw-tick")!=null){
    IW.Ticker.push({element:el,action:el.getAttribute("iw-tick")})
  }
  if(el.getAttribute("iw-pos-y")!=null){
    switch (el.getAttribute("iw-pos-y")) {
      case "middle":

        let rheight=rootElem.getBoundingClientRect().height;
        let eheight=el.getBoundingClientRect().height;
        console.log(rheight);
        console.log(eheight);
        rheight=Math.round(rheight)
        eheight=Math.round(eheight)
        console.log(rheight);
        console.log(eheight);
        let pt=Math.round(rheight/2)
        console.log(pt);
        el.style.top=pt/4+"px";
        break;
      default:

    }
  }
  if(el.getAttribute("iw-pos-x")!=null){
    switch (el.getAttribute("iw-pos-x")) {
      case "middle":

        let rheight=rootElem.getBoundingClientRect().width;
        let eheight=el.getBoundingClientRect().width;
        console.log(rheight);
        console.log(eheight);
        rheight=Math.round(rheight)
        eheight=Math.round(eheight)
        console.log(rheight);
        console.log(eheight);
        let pt=Math.round(rheight/2)
        console.log(pt);
        el.style.left=pt/4+"px";
        break;
      default:

    }
  }
  }

}
IW.DefineAction=function IWDefineAction(actionName,actionFunc){
  IW.Actions[actionName]=actionFunc
}
IW.StartTicker=function IWStartTicker(){
  let inter=setInterval(()=>{
    IW.Ticker.forEach((tick)=>{
      if(!IW.Actions[tick.action]){
        throw Error("This action isn't defined!")
        clearInterval(inter)
      }
      IW.Actions[tick.action](tick.element)
    })
  },30)

}
function q(el){
  return document.querySelector(el);
}
