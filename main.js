let io=require("socket.io")()
let recaptchaKey="6LdO19YUAAAAADIj6bSGCCVcyT6BqsEvzbzSKlAN"
let mongoCli = require("mongodb").MongoClient;
let mongo = new mongoCli("mongodb://root:Hippothebest1@mongodb.forum-miceve.svc.cluster.local:27017/", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
let db;
const makeid = length => {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}
mongo.connect((err, dbhost) => {
    if (err) {
        return console.log(err);
    }
    db = dbhost.db("lcoua");
});
io.listen(8080)
io.of("/api").on("connection",(socket)=>{
    socket.on("get-posts",()=>{
        db.collection("posts").find({}).toArray((err,arr)=>{
        socket.emit("posts",arr)
        })
    })
    socket.on("getkey",(email,password)=>{
        db.collection("users").findOne({email:email,password:password},(err,doc)=>{
            if(doc==null){
                socket.emit("apikey",true)
            }else{
                let key=makeid(50)
                db.collection("users").findOneAndUpdate({email:email,password:password},{$set:{key:key}})
                socket.emit("apikey",key)

            }
        })
    })
    socket.on("auth",(apiKey)=>{
        db.collection("users").findOne({key:apiKey},(err,doc)=>{
            if(doc==null){return}
            socket.user=doc.username
        })
    })
})
